<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function brand(){
        return $this->belongsTo(Brand::class, 'brand', 'id');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'google_product_category', 'id');
    }

    public function type(){
        return $this->belongsTo(ProductType::class, 'product_type', 'id');
    }
}
