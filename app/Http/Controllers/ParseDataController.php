<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Product;
use App\ProductType;
use Illuminate\Http\Request;

class ParseDataController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 300);
    }

    public function index()
    {
        $filename = 'feed v0.1.txt';
        $file_pointer = fopen(public_path('temp_files/' . $filename), "r");
        while ($line = fgetcsv($file_pointer, 0, "\t")) {
            $lines[] = $line;
        }
        fclose($file_pointer);
        return view('parse-data', compact('lines'));
    }

    public function parseData()
    {
        try {
            $url = 'https://s3.amazonaws.com/sando-file-feed/mNWYzZrKpqqbpZehmQ/advertiser/feed.txt';
            $contents = file_get_contents($url);
            $file = file_put_contents(public_path('/temp/file.txt'), $contents);

            if ($file) {
                $filename = 'file.txt';
                $file_pointer = fopen(public_path('temp/' . $filename), "r");
                $data = [];
//                $categories = [];
                while ($line = fgetcsv($file_pointer, 0, "\t")) {
//                    $categories[][0] = $line[9];
                    $data[] = $line;
                }
                fclose($file_pointer);
                return $this->insertProduct($data);
            }
//            return $this->insertCategory($categories);
        } catch (\Exception $e) {
            $message = [
                'info' => 'Whoops! Failed to upload file.',
                'exception' => $e->getMessage(),
            ];
            return response()->json(['success' => false, 'message' => $message]);
        }
    }

    public function insertCategory($categories)
    {
        $parent_id = null;
        $inserted_id = null;
        foreach ($categories as $cat) {
            $check = Category::where('title', $cat)->first();
            if (!$check) {
                $inserted = Category::create([
                    'title' => $cat,
                    'parent_id' => $parent_id,
                    'relations' => '',
                ]);
                if ($inserted) {
                    $inserted_id = $parent_id = $inserted->id;
                    $need_update = Category::find($inserted_id);
                    if ($need_update->parent_id == null) {
                        $need_update->update([
                            'relations' => $need_update->id,
                        ]);
                    } else {
                        $need_update->update([
                            'relations' => $need_update->id . ',' . Category::find($need_update->parent_id)->relations,
                        ]);
                    }
                }
            } else {
                $parent_id = $check->id;
            }
        }
        return $inserted_id;
    }

    public function getCategoryId($categories_string)
    {
        $categories = explode(" > ", $categories_string);
        $category = $categories[sizeof($categories) - 1];
        $result = Category::where('title', $category)->first();
        if (!$result){
            return $this->insertCategory($categories);
        } else {
            return $result->id;
        }
    }

    public function insertType($types)
    {
        $parent_id = null;
        $inserted_id = null;
        foreach ($types as $type) {
            $check = ProductType::where('title', $type)->first();
            if (!$check) {
                $inserted = ProductType::create([
                    'title' => $type,
                    'parent_id' => $parent_id,
                    'relations' => '',
                ]);
                if ($inserted) {
                    $inserted_id = $parent_id = $inserted->id;
                    $need_update = ProductType::find($inserted_id);
                    if ($need_update->parent_id == null) {
                        $need_update->update([
                            'relations' => $need_update->id,
                        ]);
                    } else {
                        $need_update->update([
                            'relations' => $need_update->id . ',' . ProductType::find($need_update->parent_id)->relations,
                        ]);
                    }
                }
            } else {
                $parent_id = $check->id;
            }
        }
        return $inserted_id;
    }

    public function getTypeId($product_type_string)
    {
        if ($product_type_string != ''){
            $types = explode(" > ", $product_type_string);
            $type = $types[sizeof($types) - 1];
            $result = ProductType::where('title', $type)->first();
            if (!$result){
                return $this->insertType($types);
            } else {
                return $result->id;
            }
        }else {
            return null;
        }
    }

    public function getBrandId($brand)
    {
        $check = Brand::where('title', $brand)->first();
        if (!$check) {
            $inserted = Brand::create([
                'title' => $brand,
            ]);
            if ($inserted) {
                return $inserted->id;
            }
        }else {
            return $check->id;
        }
    }

    public function insertProduct($data)
    {
        $time_start = microtime(true);
        $success = false;
        $row_count = 0;
        foreach ($data as $key => $product) {
            if ($key > 0) {
                $row_count++;
                $categories_string = $product[9];
                $product_type_string = $product[10];
                $cat_id = $this->getCategoryId($categories_string);
                $type_id = $this->getTypeId($product_type_string);
                $brand_id = $this->getBrandId($product[11]);
                $price_arr = explode(' ', $product[6]);
                $col = -1;
                $inserted_product = Product::create([
                    'product_code' => $product[$col+=1],
                    'title' => $product[$col+=1],
                    'description' => $product[$col+=1],
                    'link' => $product[$col+=1],
                    'image_link' => $product[$col+=1],
                    'availability' => $product[$col+=1],
                    'price' => $price_arr[0],
                    'currency' => $price_arr[1],
                    'sale_price' => $product[$col+=2],
                    'sale_price_effective_date' => $product[$col+=1],
                    'google_product_category' => $cat_id,
                    'product_type' => $type_id,
                    'brand' => $brand_id,
                    'gtin' => $product[$col+=4],
                    'mpn' => $product[$col+=1],
                    'condition' => $product[$col+=1],
                    'age_group' => $product[$col+=1],
                    'color' => $product[$col+=1],
                    'gender' => $product[$col+=1],
                    'material' => $product[$col+=1],
                    'pattern' => $product[$col+=1],
                    'size' => $product[$col+=1],
                    'item_group_id' => $product[$col+=1],
                    'promotion_id' => $product[$col+=1],
                    'custom_label_0' => $product[$col+=1],
                    'custom_label_1' => $product[$col+=1],
                    'custom_label_2' => $product[$col+=1],
                    'custom_label_3' => $product[$col+=1],
                    'custom_label_4' => $product[$col+=1],
                    'shipping' => $product[$col+=1],
                    'shipping_label' => $product[$col+=1],
                    'shipping_weight' => $product[$col+=1],
                    'shipping_length' => $product[$col+=1],
                    'shipping_width' => $product[$col+=1],
                    'shipping_height' => $product[$col+=1],
                ]);
                if ($inserted_product){
//                    return $inserted_product; // For Debugging purpose
                    $success = true;
                }else {
                    $success = false;
                }
            }
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        if ($success) {
            $message = 'Data Successfully inserted';
        } else {
            $message = 'Whoops! Data insertion failed!';
        }
        return response()->json(['success' => $success, 'no_of_rows_in_the_file' => $row_count, 'message' => $message, 'execution_time' => $execution_time . ' Seconds']);
    }

    public function test()
    {
        //
    }
}
