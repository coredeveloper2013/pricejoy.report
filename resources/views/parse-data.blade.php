@extends('app')

@section('content')
    <table class="table table-striped table-bordered table-sm table-responsive">
        @foreach($lines as $key => $line)
            @if ($key==0)
                <thead>
                <tr>
                    <th scope="col">#</th>
                    @for ($i = 0; $i < 33; $i++)
                        <td>{{ $line[$i] }}</td>
                    @endfor
                </tr>
                </thead>
            @else
                @if ($key==1)
                    <tbody>
                    @endif
                    <tr>
                        <td>{{ $key }}</td>
                        @for ($i = 0; $i < 33; $i++)
                            <td>{{ $line[$i] }}</td>
                        @endfor
                    </tr>
                    @endif
                    @endforeach
                    </tbody>
    </table>
@stop
