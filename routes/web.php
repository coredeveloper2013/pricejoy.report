<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'ParseDataController@index');
Route::get('/cat', 'ParseDataController@insertCategory');
Route::get('/parse', 'ParseDataController@parseData');
Route::get('/insert', 'ParseDataController@insertProduct');
Route::get('cron-jobs', 'CronJobsController@index');
Route::post('cron-jobs', 'CronJobsController@store');
Route::post('cron-jobs/change-status', 'CronJobsController@updateStatus')->name('change-status');
Route::post('cron-jobs/upload-csv', 'CronJobsController@uploadCSV')->name('upload-csv');
Route::get('cron-jobs/parse', 'CronJobsController@parseData');

Route::get('/ss', function (){
    return App\Brand::with('products')->first();
});
use App\Brand;
use App\Http\Resources\Brand as BrandResource;
use App\Http\Resources\BrandCollection;
Route::get('/dd', function (){
    return new BrandCollection(Brand::paginate(10));
});
